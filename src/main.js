import Vue from "vue";
import App from "./App.vue";

Vue.config.productionTip = false;

import {
  BForm,
  BFormInput,
  BFormTextarea,
  BButton,
  BListGroup,
  BFormGroup,
  BootstrapVueIcons,
} from "bootstrap-vue";

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

Vue.component("BButton", BButton);
Vue.component("BForm", BForm);
Vue.component("BFormInput", BFormInput);
Vue.component("BFormTextarea", BFormTextarea);
Vue.component("BListGroup", BListGroup);
Vue.component("BFormGroup", BFormGroup);
Vue.use(BootstrapVueIcons);

new Vue({
  el: "#app",
  render: (h) => h(App),
});
